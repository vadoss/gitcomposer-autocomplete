# What is this? #

Bash script for gitcomposer autocomplete

### Setup ###

* git clone git@bitbucket.org:vadoss/gitcomposer-autocomplete.git gc_autocomplete
* cd gc_autocomplete
* sudo cp gitcomposer /etc/bash_completion.d/gitcomposer
* . /etc/bash_completion.d/gitcomposer